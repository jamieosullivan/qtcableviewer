Writing a QT frontend for the LAMP Cable Viewer I wrote previously. The frontend uses the same database as the PHP/Ajax system (http://www.jamieos.info/projects/gate/index.php?ref=bookmarks&cableid=z)

Currently the main section for pulling up a cable using dropdown selection lists is working, with a preview showing each time the date is changed. 

![Alt text](screenshot.png)
![Alt text](demo.gif)

TODO:
 - Implement comment box
 - Implement wordcount for comment box
 - Implement sharing options
 - Implement splash/introduction screen
 - Work on overall structure and positioning of elements
 - Build rpm and exe installer (need to consider db access)
