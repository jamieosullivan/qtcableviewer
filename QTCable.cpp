#include <QtGui>
#include "qtcable.h"
//#include <stdio.h>
#include <iostream>
//#include <string>

QtCableView::QtCableView(QWidget *parent) : QDialog(parent) 
{
//    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");

//    locationBox = new QComboBox(tr("Select a location"));   // TODO Need to make new constructors to do this?
    locationBox = new QComboBox();   // TODO Need to make new constructors to do this?
    locationBox->setEditable(false);			    // Also, initial values provided in constructor or
    dbGetLocations(locationBox);
							    // a function...?
//    dateBox = new QComboBox(tr("Date.."));		    
    dateBox = new QComboBox();		    
    dateBox->setEditable(false);
    dateBox->setMinimumContentsLength(10);
    dateBox->addItem("Date..");
    
    getCableButton = new QPushButton(tr("Get cable.."));    
    getCableButton->setDefault(true);			    // Not sure what this value is needed for yet
    getCableButton->setEnabled(false);			    // Button won't be enabled til valid location and 
							    // date selected
    cableIdLabel = new QLabel(tr("Cable ID:"));		    
    cableStatusLabel = new QLabel(tr("Status:"));
    cableSummaryLabel = new QLabel(tr("Cable Summary:"));
    cableIdText = new QLabel();		    
    cableStatusText = new QLabel();
    cableSummaryText = new QLabel();
    cableTextDisplay = new QTextEdit();	    // TODO Code/files for initial text to appear in these text areas
    cableCommentBox = new QTextEdit();

   // lineEdit = new QLineEdit; // TODO remaining characters box, keeping this code
   // label->setBuddy(lineEdit);

    // When a location is selected from the Location box, the available cable dates are retrieved
    connect(locationBox, SIGNAL(currentIndexChanged(const QString &)),
	    this, SLOT(getDates(const QString &)));

    // When a date is selected, the summary for cable from the corresponding date and location is retrieved
    // and displayed in the "Cable Summary" QLabel
    connect(dateBox, SIGNAL(currentIndexChanged(const QString &)),
	    this, SLOT(displaySummary(const QString &)));

    // When the "Get Cable" button is clicked, the cable for the corresponding location and date is retrieved
    // and displayed in the cableTextDisplay text area
    connect(getCableButton, SIGNAL(clicked()),
	    this, SLOT(getCable()));
	  //  this, SLOT(getCable(const QString &, const QString &)));

    // Top row - contains location box, date box and "Get Cable" button
    QHBoxLayout *topLayout = new QHBoxLayout;
    topLayout->addWidget(locationBox);
    topLayout->addWidget(dateBox);
    topLayout->addWidget(getCableButton);

    // 2nd row - contains cable ID and status labels
    QHBoxLayout *secondRowLayout = new QHBoxLayout;
    secondRowLayout->addWidget(cableIdLabel);
    secondRowLayout->addWidget(cableIdText);
    secondRowLayout->addWidget(cableStatusLabel);
    secondRowLayout->addWidget(cableStatusText);

    // 3rd row - contains the cable summary label
    QHBoxLayout *thirdRowLayout = new QHBoxLayout;
    thirdRowLayout->addWidget(cableSummaryLabel);
    thirdRowLayout->addWidget(cableSummaryText);

    // Add the top three horizontal layouts to a vertical layout, and try to add the text boxes directly
    // to the vertical layout
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(topLayout);
    mainLayout->addLayout(secondRowLayout);
    mainLayout->addLayout(thirdRowLayout);
    mainLayout->addWidget(cableTextDisplay);
    mainLayout->addWidget(cableCommentBox);

    setLayout(mainLayout);

    setWindowTitle(tr("Cable Viewer"));
    setFixedHeight(sizeHint().height());
}

void QtCableView::getDates(const QString orig) {
    QString datequery;
    QTextStream ts(&datequery);
    ts << "select cab_date from cable_dates where cab_origin='" << orig \
	    << "' order by cab_date desc"; 
    QSqlQuery query(datequery);
    QStringList slist, titleSlist;
    while(query.next()){
	QString currentdate = query.value(0).toString();
	slist << currentdate;
    }
    slist.sort();
    titleSlist << slist;
    dateBox->clear();		    // Trouble here with summary? May need to 
				    // check value in displaySummary()
    dateBox->addItems(titleSlist);

    // Activate our get cable button
    getCableButton->setEnabled(true);			   

}


void QtCableView::displaySummary(const QString date) {
    QString idquery;
    QTextStream ts(&idquery);

    // 1st query - Get the cable ID corresponding to the selected location and date
    ts << "select cab_id from cable_dates where cab_origin='" << locationBox->currentText() \
      << "' and cab_date='" << date << "'";	
    QSqlQuery query(idquery);
    std::cout << "query.next() retval: " << query.next() << endl;

   // QString cabid = query.value(0).toString();
    cabID = query.value(0).toString();
    std::cout << "cabid = " << cabID.toStdString();
    //ts.seek(0);

    // 2nd query - Select the Summary corresponding to the retrieved Cable ID
    QString idquery2;
    QTextStream ts2(&idquery2);
    ts2 << "select cab_class, cab_summary from cable_summary where cab_id='" << cabID << "'";
    QSqlQuery query2(idquery2);
    std::cout << "query2.next() retval: " << query2.next() << endl;

    // Update the Cable Summary label
    QString cabclass = query2.value(0).toString();
    QString cabsummary = query2.value(1).toString();

    cableIdText->setText(cabID);		    
    cableStatusText->setText(cabclass);
    cableSummaryText->setText(cabsummary);

}



//void QtCableView::getCable(const QString &, const QString &) {
void QtCableView::getCable() {
    QString querytext;
    QTextStream ts(&querytext);
    qDebug() << "*** cabID: " << cabID << endl;

    ts << "select cab_text from cables where cab_id='" << cabID << "'";
    QSqlQuery query(querytext);

    std::cout << "query.next() retval: " << query.next() << endl;

    QString cabtext = query.value(0).toString();

    qDebug() << cabtext ;
    cableTextDisplay->setPlainText(cabtext);

}


/*
void FindDialog::findClicked()
{
    QString text = lineEdit->text();
    Qt::CaseSensitivity cs = 
	caseCheckBox->isChecked()   ? Qt::CaseSensitive
				    : Qt::CaseInsensitive;
    if (backwardCheckBox->isChecked()) {
	emit findPrevious(text, cs);
    } else {
	emit findNext(text, cs);
    }
}

void FindDialog::enableFindButton(const QString &text)
{
    findButton->setEnabled(!text.isEmpty()); 
}
*/
// TODO probably want string variables specified in a config file to avoid hardcoded values below
bool createConnection()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setDatabaseName("hellsgate");
    db.setUserName("app");
    db.setPassword("ezpz");
    if(!db.open()) {
//	QMessageBox::critical(0, QObject::tr("Database Error"), QObject::tr("2nd message"),db.lastError());
	QMessageBox::critical(0, QObject::tr("Database Error"), QObject::tr("1st message"));
	return false;
    }
    return true;
}

void dbGetLocations(QComboBox *locations)
{
    QSqlQuery query("select origin from cable_origins");
    QStringList slist, titleSlist;
    titleSlist << "Select a location..";
   // printf("Number of locations: %d\n", query.size());
    while(query.next()){
	QString currentloc = query.value(0).toString();
	slist << currentloc;
//	locations->addItem(currentloc);
    }
    slist.sort();
    titleSlist << slist;
    locations->addItems(titleSlist);
   // locations->addItems(slist);
	
}

