#include <QApplication>
#include "qtcable.h"

int main(int argc, char *argv[]) 
{
    QApplication app(argc, argv);

    if (!createConnection()) {
	return 1;
    }

    QtCableView *view = new QtCableView;

    view->show();
    return app.exec();
}
