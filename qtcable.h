#ifndef QTCABLE_H
#define QTCABLE_H

#include <QDialog>
#include <QComboBox>
#include <QMessageBox>
#include <QtSql/QSqlDatabase>
#include <QtSql/QMYSQLDriver>
#include <QSqlQuery>


class QComboBox;
class QMessageBox;
class QPushButton;
class QLabel;
class QTextEdit;
class QSqlError;

bool createConnection();
void dbGetLocations(QComboBox *);


class QtCableView : public QDialog 
{
    Q_OBJECT
    public:
	QtCableView(QWidget *parent = 0);

    private slots:
	// TODO Review arguments - Sketch these for the moment
	void getDates(const QString location);	// Retrieve dates for which cables exist from this location
//	void displaySummary(const QString &location, const QString &date);  // Display summary for cable selected
	void displaySummary(const QString date);  // Display summary for cable selected
	//void getCable(const QString &location, const QString &date);	    // Retrieve full cable
	void getCable();	    // Retrieve full cable

    private:
	QString cabID;
	QComboBox *locationBox;		// Combobox to select location
	QComboBox *dateBox;		// Combobox to select date (i.e. a particular cable)
	QPushButton *getCableButton;	// Button to retrieve full cable
	QLabel *cableIdLabel;		// Cable Id displayed in summary
	QLabel *cableStatusLabel;	// Cable status (security rating) displayed in summary
	QLabel *cableSummaryLabel;	// Cable summary text
	QLabel *cableIdText;		// Cable Id displayed in summary
	QLabel *cableStatusText;	// Cable status (security rating) displayed in summary
	QLabel *cableSummaryText;	// Cable summary text

	QTextEdit *cableTextDisplay;	// Text box to display full cable text
	QTextEdit *cableCommentBox;	// Text box for comment to be tweeted etc.

	// TODO Can add another box for remaining characters, and corresponding labels

};



#endif
